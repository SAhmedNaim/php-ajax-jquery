$(document).ready(function(){ 

	// check username availability
	$('#username').blur(function(){
		var username = $(this).val();
		$.ajax({
			url:"check/checkuser.php",
			method:"POST",
			data:{username:username},
			dataType:"text",
			success:function(data){
				$('#userstatus').html(data);
			}
		});
	});  

	// autocomplete password
	$('#skill').keyup(function(){
		var skill = $(this).val();
		if (skill != '') {
			$.ajax({
				url:"check/checkskill.php",
				method:"POST",
				data:{skill:skill},
				dataType:"text",
				success:function(data){
					$('#skillstatus').fadeIn();
					$('#skillstatus').html(data);
				}
			});
		};
	});

	$(document).on('click', 'li', function(){
		$('#skill').val($(this).text());
		$('#skillstatus').fadeOut();
	});

	// show password
	$("#showpassword").on('click', function(){
		var pass = $("#password");
		var fieldtype = pass.attr('type');
		if (fieldtype == 'password') {
			pass.attr('type', 'text');
			$(this).attr('value', 'Hide Password');
		} else {
			pass.attr('type', 'password');
			$(this).attr('value', 'Show Password');
		}
		return false;
	});

	// auto refresh div content
	$("#autosubmit").click(function(){
		var content = $("#body").val();
		if ($.trim(content) != '') {
			$.ajax({
				url:"check/checkrefresh.php",
				method:"POST",
				data:{body:content},
				dataType:"text",
				success:function(data){
					$("#body").val("");
				}
			});
			return false;
		}
	});

	setInterval(function(){
		$("#autostatus").load("check/getrefresh.php").fadeIn("slow");
	}, 1000);

	// live data search
	$("#livesearch").keyup(function(){
		var live = $(this).val();
		if (live != '') {
			$.ajax({
				url:"check/livesearch.php",
				method:"POST",
				data:{search:live},
				dataType:"text",
				success:function(data){
					$('#livestatus').html(data);
				}
			});
		} else {
			$('#livestatus').html("");
		}
	});

	// Auto save data
	function autoSave() {
		var content = $("#content").val();
		var contentid = $("#contentid").val();
		if (content != null) {
			$.ajax({
				url:"check/autosave.php",
				method:"POST",
				data:{content:content,contentid:contentid},
				dataType:"text",
				success:function(data){
					if (data != '') {
						$('#contentid').val(data);
					}
					$('#saveStatus').text("Content Saved As Draft...");
					setInterval(function(){
						$('#saveStatus').text("");
					}, 2000);
				}
			});
			return false;
		}
	}

	setInterval(function(){
		autoSave();
	}, 10000);

});  