-- phpMyAdmin SQL Dump
-- version 4.6.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 13, 2017 at 07:07 AM
-- Server version: 5.7.14
-- PHP Version: 5.6.25

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_jquery`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_search`
--

CREATE TABLE `tbl_search` (
  `id` int(11) NOT NULL,
  `username` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL
) ENGINE=MyISAM DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_search`
--

INSERT INTO `tbl_search` (`id`, `username`, `name`, `email`) VALUES
(1, 'imran', 'Imran Hossain', 'imran@gmail.com'),
(2, 'naim', 'S Ahmed Naim', 'naim@gmail.com'),
(3, 'jhoty', 'Jhoty Sarker', 'jhoty@gmail.com'),
(4, 'limon', 'Limon Sarker', 'limon@gmail.com'),
(5, 'yasir', 'Yasir Arafat', 'yasir@gmail.com'),
(6, 'alamgir', 'Alamgir Hossain', 'alamgir@gmail.com'),
(7, 'nobin', 'Nobin Khan', 'nobin@gmail.com'),
(8, 'ali', 'Mohammad Ali', 'ali@gmail.com'),
(9, 'ariful', 'Ariful Islam', 'ariful@gmail.com'),
(10, 'abir', 'Abir Ahmed', 'abir@gmail.com');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_search`
--
ALTER TABLE `tbl_search`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_search`
--
ALTER TABLE `tbl_search`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
