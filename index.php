<?php include 'inc/header.php'; ?>
<h2>Topics: Project List</h2>
<div class="content">
	<div class="topics">
		<ul>
			<li><a href="user.php" target="_blank">01. Check Username Availability</a></li>
			<li><a href="textbox.php" target="_blank">02. Autocomplete Textbox</a></li>
			<li><a href="password.php" target="_blank">03. Create A Show Password</a></li>
			<li><a href="refresh.php" target="_blank">04. Auto Refresh Div Content</a></li>
			<li><a href="livesearch.php" target="_blank">05. Ajax Live Data Search</a></li>
			<li><a href="autosave.php" target="_blank">06. Auto Save Data</a></li>
		</ul>
	</div>
</div>
<?php include 'inc/footer.php'; ?>