<?php include 'inc/header.php'; ?>
<h2>Autocomplete textbox</h2>
<div class="content">
	
	<style>
		.skill {background-color: #fba991; margin-left: 51px; padding: 6px 30px; width: 190px;}
		.skill ul {margin: 0px; padding: 0px; list-style: none;}
		.skill ul li{cursor: pointer;}
	</style>

	<form action="" method="post">
		<table>
			<tr>
				<td>Skill</td>
				<td>:</td>
				<td><input type="text" name="skill" id="skill" placeholder="Enter your skill. . ."/></td>
			</tr>
		</table>
		<div id="skillstatus"></div>
	</form>
</div>
<?php include 'inc/footer.php'; ?>